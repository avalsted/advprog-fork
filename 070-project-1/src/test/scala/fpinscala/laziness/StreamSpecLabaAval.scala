package fpinscala.laziness

import scala.language.higherKinds
import org.scalatest.FlatSpec
import org.scalatest.prop.Checkers
import org.scalacheck._
import org.scalacheck.Prop._
import Arbitrary.arbitrary
import scala.util.Random

// ***********************************************
//  070-Project 1
//  Aval & Laba
// ***********************************************

//Stream imports
 import stream00._      // Uncomment to test the book solution
// import stream01._    // Uncomment to test the broken headOption implementation
// import stream02._    // Uncomment to test another version that breaks headOption

class StreamSpecLabaAval extends FlatSpec with Checkers {
    import Stream._

/* GENERATORS */
    def list2stream[A] (la :List[A]): Stream[A] = la.foldRight (empty[A]) (cons[A](_,_))

    def genNonEmptyList[A] (implicit arbA :Arbitrary[A]): Gen[List[A]] =
        for { la <- arbitrary[List[A]] suchThat (_.nonEmpty) } yield la

    def genNonEmptyStream[A] (implicit arbA :Arbitrary[A]): Gen[Stream[A]] =
        for { la <- arbitrary[List[A]] suchThat (_.nonEmpty) }
        yield list2stream (la)

    def genFiniteStream[A] (n: Int) (implicit arbA: Arbitrary[A]): Gen[Stream[A]] = 
        for { la <- Gen.listOfN(n, arbitrary(arbA)) }
        yield list2stream (la)

    def genInfiniteIntStream: Stream[Int] = {
        val r = scala.util.Random
        cons(r.nextInt, genInfiniteIntStream)
    }

    def genExceptionStream[A] (implicit arbA: Arbitrary[A]): Gen[Stream[A]] = {
        for { la <- genNonEmptyStream[A] } 
        yield cons((throw new RuntimeException("Auch!")), la)
    }


    implicit val errorEvalGen: Arbitrary[ErrorEval[Int]] = Arbitrary {
        for {
            i <- Gen.choose(-10000, 10000)
        } yield new ErrorEval(i)
    }

/* Helper Objects */
class ErrorEval[A] (notYetEvaluated: => A) {
    var hasBeenEvaluated = false
    lazy val v: A = {
        hasBeenEvaluated = true
        10/0            //Throws exception if evaluated
        notYetEvaluated
    }
}

behavior of "headOption" // --------------------------------------------------------------------

    it should "return None on an empty Stream" in {
      assert(empty.headOption == None)
    }

    it should "return the head of the stream packaged in Some" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        ("singleton" |:
          Prop.forAll { (n :Int) => cons (n,empty).headOption == Some (n) } ) &&
        ("random" |:
          Prop.forAll { (s :Stream[Int]) => s.headOption != None } )
    }

    it should "not force evaluation of the tail" in {
        val s = cons(new ErrorEval(42), cons(new ErrorEval(43), empty))  
        s.headOption //If headOption forced the tail, an exception would be thrown.
    }

behavior of "take" // --------------------------------------------------------------------

    it should "not force any heads nor any tails of the Stream it manipulates" in check {  
        implicit def arbErrorValStream = Arbitrary[Stream[ErrorEval[Int]]] (genNonEmptyStream[ErrorEval[Int]])
        forAll { (n: Int, s :Stream[ErrorEval[Int]]) => 
            val ho = s.take(n).headOption
            ho match { 
                case Some(h) => h.hasBeenEvaluated == false 
                case None    => true // Needed for take(0)
            }
        }
    }

    it should "s.take(n).take(n) == s.take(n) for any Stream s and any n" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, s: Stream[Int]) => s.take(n).take(n).toList == s.take(n).toList }
    }

    //[] - Take(n) does not force (n+1)st head ever (even if we force all elements of take(n))
    //TODO

    it should "take(n) from an empty Stream should return empty Stream" in check {
        forAll { (n: Int) => empty.take(n) == empty }
    }

    it should "take(n) from same Stream twice yields the same two substreams" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, s: Stream[Int]) => 
                    val first = s.take(n).toList
                    val second = s.take(n).toList
                    first == second 
        }
    }

    it should "take(n) where n is negative or 0 should yield an empty Stream" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, s: Stream[Int]) => (n <= 0) ==> (s.take(n) == empty) }
    }

    it should "take(n) from a Stream of length < n should yield all available elements in the Stream" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (s: Stream[Int]) =>
                     val strm = s.toList
                     val size = strm.size
                     val strm1 = s.take(size+1).toList
                     strm == strm1
        }
    }


//For drop: (AVAL)
behavior of "drop"

    it should "be additive: s.drop(n).drop(m) == s.drop(n+m)" in check {
        implicit def boundedInt = Arbitrary[Int] (Gen.choose(0, 1000000))
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, m: Int, s: Stream[Int]) 
            => (s.drop(n).drop(m).toList == s.drop(n+m).toList) }
    }

    it should "be commutative: s.drop(n).drop(m) == s.drop(m).drop(n)" in check {
        implicit def boundedInt = Arbitrary[Int] (Gen.choose(0, 1000000))
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, m: Int, s: Stream[Int]) 
            => (s.drop(n).drop(m).toList == s.drop(m).drop(n).toList) }
    }
    
    it should "not force head (throw exception) for s.drop(1) (Scenario test)" in {
        lazy val s: Stream[Int] = cons (throw new RuntimeException("FORCE!"), cons (100, empty) )
        assert(s.drop(1).toList == List(100))
    }

    it should "not force any elements heads" in {
        lazy val s: Stream[Int] = 
            cons(throw new RuntimeException("FORCE_1"), 
            cons(throw new RuntimeException("FORCE_2"),
            cons(throw new RuntimeException("FORCE_3"),
            cons(throw new RuntimeException("FORCE_4"), cons(42, empty)))))
        s.drop(1) // should not fail
        s.drop(2) // should not fail
        assert(s.drop(4).headOption == Some(42))
    } 

    it should "not force heads even if tail elements is forced" in {
        lazy val strm: Stream[Int] = cons(1, cons(
                (throw new RuntimeException("FAIL 1")), cons(3, empty)))
        assert(strm.drop(2).headOption == Some(3))
        assert(strm.drop(3).headOption == None)
    }

    it should "Drop(n) of a finite Stream of length < n should be empty" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        implicit def boundedInt = Arbitrary[Int] (Gen.choose(1, 100000))
        forAll { (n: Int, s: Stream[Int]) => 
                    val strmSize = s.toList.size
                    s.drop(strmSize+n) == empty
        }
    }

    it should "Drop(n) independently from the same Stream twice should yield the same Stream" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, s:Stream[Int]) => s.drop(n) == s.drop(n) }
    }

    it should "leave Stream untouched if n is <= 0 for s.drop(n)" in check {
        implicit def boundedInt = Arbitrary[Int] (Gen.choose(-1000000, 0))
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (n: Int, s:Stream[Int]) => 
                    val strm = s.drop(n)
                    strm.toList == s.toList 
        }
    }

behavior of "map" // --------------------------------------------------------------------

    it should "return an identical Stream when given the identity function" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (s: Stream[Int]) => s.map(x => x).toList == s.toList }
    }

    it should "terminate on infinite Streams" in {
        genInfiniteIntStream.map(x => x)
        assert(true) //If the above terminates, the test should pass
    }

    it should "return an empty Stream if evaluated on an empty Stream" in {
        assert(empty[Int].map(x => x.toString) == empty)
    }

    it should "apply functions correctly on Streams" in {
        val s1 = cons(21, cons(44, empty))
        val s2 = cons(42, cons(88, empty))
        assert(s1.map(x => x*2).toList == s2.toList)
        assert(s2.map(x => x/2).toList == s1.toList)
    }

    it should "maintain the length of a Stream after a .map() has been applied" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        forAll { (s: Stream[Int]) => 
                    val size  = s.toList.size
                    val size1 = s.map(x => x.toString).toList.size 
                    size == size1
        }
    }

behavior of "append" // --------------------------------------------------------------------

    it should "leave stream untouched when appending nothing" in {
        lazy val s: Stream[Int] = cons (1, cons (2, empty))
        lazy val app: Stream[Int] = empty
        assert(s.append(app).toList == s.toList)
    }

    it should "not alter or discard elemnents of the stream when appending (empty case)" in {
        lazy val s1 = list2stream(List.empty)
        lazy val s2 = list2stream(List.empty)
        s1.append(s2).toList == List.empty
    }

    it should "not reorder or discard elements" in check {
        implicit def arbIntList = Arbitrary[List[Int]] (genNonEmptyList[Int])
        forAll { (l1: List[Int], l2: List[Int]) =>
            lazy val s1 = list2stream(l1)
            lazy val s2 = list2stream(l2)
            s1.append(s2).toList == l1 ++ l2
        }
    }

    it should "yield a stream n elements longer when appending stream of n" in check {
        forAll { (l1: List[Int], l2: List[Int]) =>
            val len = l1.size + l2.size
            lazy val s1 = list2stream(l1)
            lazy val s2 = list2stream(l2)
            s1.append(s2).toList.size == len
        }
    }

    it should "not force evaluation of either stream" in {
        lazy val s1: Stream[Int] = cons (1, cons ((throw new RuntimeException("FAIL 1")), cons (3, empty)))
        lazy val s2: Stream[Int] = cons (1, cons ((throw new RuntimeException("FAIL 2")), empty))
        s1.append(s2)
        true //Exception is thrown before this statement if either Stream is evaluated
    }

    it should "terminate when appending infinite streams" in check {
        implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
        lazy val inf = genInfiniteIntStream
        forAll { (s: Stream[Int]) => s.append(inf); true }
    }
}