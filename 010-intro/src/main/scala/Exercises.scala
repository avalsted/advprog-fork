// Advanced Programming, Exercises by A. Wąsowski, IT University of Copenhagen
//
// AUTHOR1:
// AUTHOR2:
//
// Write ITU email addresses of both group members that contributed to
// the solution of the exercise (in lexicographic order).
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// The file is meant to be compiled inside sbt, using the 'compile' command.
// To run the compiled file use the 'run' or 'runMain' command.
// To load the file int the REPL use the 'console' command.
// Now you can interactively experiment with your code.
//
// Continue solving exercises in the order presented in the PDF file. The file
// shall always compile, run, and pass tests, after you are done with each
// exercise (if you do them in order).  Please compile and test frequently.

// The extension of App allows writing the main method statements at the top
// level (the so called default constructor). For App objects they will be
// executed as if they were placed in the main method in Java.
package fpinscala

object Exercises extends App with ExercisesInterface {

  import fpinscala.List._

  // Exercise 3
  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  //@annotation.tailrec
  def fib (n: Int) : Int = {
      def go (idx: Int, cur: Int, prev: Int) : Int = idx match {
          case 0 => prev
          case x => go(idx - 1, cur + prev, cur)
      }
      go(n, 1, 0)
  }

  // Exercise 4
  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  @annotation.tailrec
  def isSorted[A] (as: Array[A], ordered: (A,A) =>  Boolean) :Boolean = {
    as match {
      case Array(a, b, _*) => if(ordered(a,b)) isSorted(as.tail, ordered) else false
      case Array(_)    => true
    }
  }

  // Exercise 5
  def curry[A,B,C] (f: (A,B)=>C): A => (B => C) = {
    (a: A) => (b: B) => f(a, b)
  }

  // Exercise 6
  def uncurry[A,B,C] (f: A => B => C): (A,B) => C = {
    (a: A, b: B) => f(a) (b)
  }

  // Exercise 7
  def compose[A,B,C] (f: B => C, g: A => B) : A => C = {
    (a: A) => f(g(a))
  }

  // Exercise 8 requires no programming

  // Exercise 9
  def tail[A] (as: List[A]) :List[A] = as match {
        case Nil        => throw new IllegalArgumentException()
        case Cons(h, t) => t
    }

  // Exercise 10
  // @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  @annotation.tailrec
  def drop[A] (l: List[A], n: Int) : List[A] = n match {
      case 0 => l
      case x => drop(tail(l), x-1)
  }

  // Exercise 11
  // @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  @annotation.tailrec
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
      case Nil       => Nil
      case Cons(h,t) => if(f(h)) dropWhile(t, f) else l
  }

  // Exercise 12
  def init[A](l: List[A]): List[A] = tail(reverse(l))

  // Exercise 13
  def length[A] (as: List[A]): Int = {
    def go(lst: List[A], acc: Int): Int = {
      lst match {
        case Nil        => acc
        case Cons(h, t) => go(t, acc + 1)
      }
    }
    go(as, 0)
  }

  // Exercise 14
  // @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  @annotation.tailrec
  def foldLeft[A,B] (as: List[A], z: B) (f: (B, A) => B): B = {
    as match {
      case Nil        => z
      case Cons(h, t) => foldLeft(t, f(z, h)) (f)
    }
  }

  // Exercise 15
  def product (as: List[Int]): Int = foldLeft (as, 1) (_ * _) 
  def length1 (as: List[Int]): Int = foldLeft (as, 0) { (a,e) => a + 1 }

  // Exercise 16
  def reverse[A] (as: List[A]): List[A] = foldLeft (as, List[A]()) { (a,e) => Cons(e,a) }

  // Exercise 17
  def foldRight1[A,B] (as: List[A], z: B) (f: (A, B) => B): B = ???
  /*{
      foldLeft(reverse(as), z) (f)
  }*/

  // Exercise 18 // awat?
  def foldLeft1[A,B] (as: List[A], z: B) (f: (B,A) => B): B = ???

  // Exercise 19
  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
    case Nil        => a2
    case Cons(h,t)  => Cons(h, append(t, a2))
  }

  def concat[A] (as: List[List[A]]): List[A] = {
      def go(ll: List[List[A]], acc: List[A]) : List[A] = ll match {
          case Nil       => acc
          case Cons(l,t) => go(t, append(acc, l))
      }
      go(as, List[A]())
  }

  // Exercise 20
  def filter[A] (as: List[A]) (f: A => Boolean): List[A] = {
      def go(l: List[A], res: List[A]) : List[A] = l match {
          case Nil        => res
          case Cons(l, t) => if(f(l)) go(t, Cons(l,res)) else go(t, res)
      }
      go(as, List[A]())
  }

  // Exercise 21
  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = ???

  // Exercise 22
  def filter1[A] (l: List[A]) (p: A => Boolean) :List[A] = ???

  // Exercise 23
  def add (l: List[Int]) (r: List[Int]): List[Int] = ???

  // Exercise 24
  def zipWith[A,B,C] (f: (A,B)=>C) (l: List[A], r: List[B]): List[C] = ???

  // Exercise 25
  def hasSubsequence[A] (sup: List[A], sub: List[A]): Boolean = ???

  // Exercise 26
  def pascal (n: Int): List[Int] = ???

}
