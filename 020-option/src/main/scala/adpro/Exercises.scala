// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 21
//
// AUTHOR1: Lauritz Baess-Lehmann
// TIME1: 5,5 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Anders Valsted
// TIME2: 5 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// This file is compiled with 'sbt compile' and tested with 'sbt test'.
//
// The file shall always compile and run after you are done with each exercise
// (if you do them in order).  Please compile and test frequently. Of course,
// some tests will be failing until you finish. Only hand in a solution that
// compiles and where tests pass for all parts that you finished.    The tests
// will fail for unfnished parts.  Comment such out.

package adpro

// Exercise  1

/* We create OrderedPoint as a trait instead of a class, so we can mix it into
 * Points (this allows to use java.awt.Point constructors without
 * reimplementing them). As constructors are not inherited, We would have to
 * reimplement them in the subclass, if classes not traits are used.  This is
 * not a problem if I mix in a trait construction time. */

trait OrderedPoint extends scala.math.Ordered[java.awt.Point] {

  this: java.awt.Point =>

  override def compare (that: java.awt.Point): Int = {
    if(this.x == that.x && this.y == that.y) 0
    else if(this.x < that.x) -1
    else if(this.x == that.x && this.y < that.y) -1
    else 1
  }
}

// Try the following (and similar) tests in the repl (sbt console):
// val p = new java.awt.Point(0,1) with OrderedPoint
// val q = new java.awt.Point(0,2) with OrderedPoint
// assert(p < q)

// Chapter 3
sealed trait Tree[+A]
case class Leaf[A] (value: A) extends Tree[A]
case class Branch[A] (left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  def size[A] (t :Tree[A]): Int = {
    def go (tr :Tree[A], cnt: Int) : Int = {
      tr match {
        case Leaf(_)        => cnt + 1
        case Branch(lt, rt) => go(lt, cnt) + 1 + go(rt, cnt)
      }
    }
    go(t, 0)
  }

  // Exercise 3 (3.26)
  def maximum (t: Tree[Int]): Int = {
    def go (tr :Tree[Int], mx :Int) : Int = {
      tr match {
        case Leaf(x)        => x max mx
        case Branch(lt, rt) => go(lt, mx) max go(rt, mx)
      }
    }
    go(t, Int.MinValue)
  }

  // Exercise 4 (3.28)
  def map[A,B] (t: Tree[A]) (f: A => B): Tree[B] = {
    def go (tr : Tree[A]): Tree[B] = tr match {
      case Leaf(a)        => Leaf(f(a))
      case Branch(lt, rt) => Branch(go(lt), go(rt))
    }
    go(t)
  }

  // Exercise 5 (3.29)
  def fold[A,B] (t: Tree[A]) (f: (B,B) => B) (g: A => B): B = t match {
    case Leaf(a)        => g(a)
    case Branch(lt, rt) => f(fold (lt) (f) (g), fold (rt) (f) (g))
  }

  def size1[A] (t: Tree[A]): Int = fold (t) ((x: Int, y: Int) => x+1+y) (_ => 1)

  def maximum1[A] (t: Tree[Int]): Int = fold (t) ((x: Int, y: Int) => x max y) (z => z)

  def map1[A,B] (t: Tree[A]) (f: A => B): Tree[B] = fold (t) ((lt: Tree[B], rt: Tree[B]) => Branch(lt, rt): Tree[B]) (a => Leaf(f(a)))
}

sealed trait Option[+A] {

  // Exercise 6 (4.1)
  def map[B] (f: A => B): Option[B] = this match {
      case None     => None
      case Some(x)  => Some(f(x))
  }

  // You may Ignore the arrow in default's type below for the time being.
  // (it should work (almost) as if it was not there)
  // It prevents the argument "default" from being evaluated until it is needed.
  // So it is not evaluated in case of Some (the term is 'call-by-name' and we
  // should talk about this soon).

  def getOrElse[B >: A] (default: => B): B = this match {
      case None     => default
      case Some(x)  => x
  }

  def flatMap[B] (f: A => Option[B]): Option[B] = this match {
      case None     => None
      case Some(x)  => f(x)
  }

  def filter (p: A => Boolean): Option[A] = this match {
    case None       => None
    case Some(x)    => if(p(x)) { Some(x) } else { None }
  }
}

case class Some[+A] (get: A) extends Option[A]
case object None extends Option[Nothing]

object ExercisesOption {

  // Remember that mean is implemented in Chapter 4 of the text book
  def mean(xs: Seq[Double]): Option[Double] = {

    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)
  }

  // Exercise 7 (4.2)
  // TODO: Try out for-comprehensions
  def variance (xs: Seq[Double]): Option[Double] = mean(xs).map {m => xs.map {x => math.pow(x - m, 2)}}.flatMap(mean(_))

  // Exercise 8 (4.3) -> Option is a collection type with one element, as such for unwraps an Option type
  def map2[A,B,C] (ao: Option[A], bo: Option[B]) (f: (A,B) => C): Option[C] =  for {
      a <- ao
      b <- bo
  } yield f(a,b)

  // Exercise 9 (4.4) -> The cons operator yields None if one of the values to be cons'ed is None
  def sequence[A] (aos: List[Option[A]]): Option[List[A]] = aos.foldRight[Option[List[A]]] (Some(Nil)) (map2 (_,_) (_::_))

  // Exercise 10 (4.5)
  def traverse[A,B] (as: List[A]) (f: A => Option[B]): Option[List[B]] = as.foldRight[Option[List[B]]] (Some(Nil)) ((a, bo) => map2 (f(a), bo) (_::_))
}

