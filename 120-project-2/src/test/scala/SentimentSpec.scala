import org.scalatest.{FreeSpec, Matchers, BeforeAndAfterAll}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset
import scala.collection.JavaConverters._

class SentimentSpec extends FreeSpec with Matchers with BeforeAndAfterAll {

  org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)

  val spark =  SparkSession.builder
    .appName ("Sentiment")
    .master  ("local[12]")
    .getOrCreate

  override def afterAll = spark.stop

  import spark.implicits._
  import Main._
  
  val d = 50
  val glove  = loadGlove ("./gloves/glove_"+d+"d.txt")

  def toParsedReview (review: Seq[(Int, String, Double)]): Dataset[ParsedReview] = {
    review.toDS()
      .withColumnRenamed ("_1", "id" )
      .withColumnRenamed ("_2", "text")
      .withColumnRenamed ("_3", "overall")
      .as[ParsedReview]
  }

  "Pre-processing tests"   - {
    // Test prediction on some sample input (with known rating - different set!)
    "Mapping ratings" - {
      "Negative remap works" in { 
        val review = toParsedReview(
          Seq((1, "SampleText", 1.0), (1, "SampleText", 2.0))
        )
          
        val mapped = mapRatings(review)
        val mapArr = mapped.head(2);

        assert(mapArr(0)._2 == 0)  // mapped to 'Negative'
        assert(mapArr(1)._2 == 0)  // mapped to 'Negative'
      } 

      "Neutral remap works" in { 
        val review = toParsedReview( Seq((1, "SampleText", 3.0)) )
          
        val mapped = mapRatings(review)

        assert(mapped.head()._2 == 1)  // mapped to 'Neutral'
      } 

      "Positive remap works" in { 
        val review = toParsedReview(
          Seq((1, "SampleText", 4.0), (1, "SampleText", 5.0))
        )
          
        val mapped = mapRatings(review)
        val mapArr = mapped.head(2);

        assert(mapArr(0)._2 == 2)  // mapped to 'Positive'
        assert(mapArr(1)._2 == 2)  // mapped to 'Positive'
      } 
    }
    
    "Extracting word-vectors" - {
      "Correctly maps existing word" in {
        // Example summaries from Automotive.json
        val reviews = toParsedReview(Seq(
          (1, "Good, Easy to Use, Quick", 5.0),
          (2, "Great for your boat bike or anything that you need to charge over night", 4.0),
          (3, "Useless. Does not really work as described", 1.0) 
        ))

        val converted = mapToWordVecs(reviews)(glove)

        // Take more than needed, to be sure everything is eval'd.
        val r1 = converted.filter(rv => rv._1 == 1).takeAsList(100)
        val r2 = converted.filter(rv => rv._1 == 2).takeAsList(100)
        val r3 = converted.filter(rv => rv._1 == 3).takeAsList(100)
        assert(r1.size == 5)
        assert(r1.size == 14)
        assert(r1.size == 7)
      }

      "Leaves out not-found words" in {
        val review = toParsedReview(Seq(
          (1, "xxiouahuf is not a real word", 1.0) // Expect no vector for 'xxiouahuf'
        ))

        val converted = mapToWordVecs(review)(glove)
        
        // Take more than needed, to be sure everything is eval'd.
        assert(converted.takeAsList(10).size == 5)
        // is + not + a + real + word = 5  (xxiouahuf should not be part of the end result rows). 
      }
    }

    "Averaging Vectors" - {
      "Calculates as expected" in { 
        val reviewVec = Seq(
          (1, List(1.0, 1.0, 1.0)),
          (1, List(2.0, 2.0, 2.0)),
          (1, List(3.0, 3.0, 3.0))
        ).toDS()
          .withColumnRenamed ("_1", "id" )
		      .withColumnRenamed ("_2", "vec")
          .as[ReviewVector]

        val avgVec = calcAvgVectors(reviewVec)

        val avg = avgVec.head()._2.toList
        assert(avg == List(2.0, 2.0, 2.0))
      } 
    } 
  }
}
