// Advanced Programming. Andrzej Wasowski. IT University
// To execute this example, run "sbt run" or "sbt test" in the root dir of the project
// Spark needs not to be installed (sbt takes care of it)

import scala.collection.immutable.List
import org.apache.spark.ml.feature.Tokenizer
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.linalg._
import org.apache.spark.ml.tuning._

object Main {

  type Embedding       = (String, List[Double])
  type ParsedReview    = (Int, String, Double)

  org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)
  val spark =  SparkSession.builder
    .appName ("Sentiment")
    .master  ("local[9]")
    .getOrCreate

  import spark.implicits._
  val reviewSchema = StructType(Array(
    StructField ("reviewText", StringType, nullable=false),
    StructField ("overall",    DoubleType, nullable=false),
    StructField ("summary",    StringType, nullable=false)))

  // Read file and merge the text abd summary into a single text column
  def loadReviews (path: String): Dataset[ParsedReview] =
    spark
      .read
      .schema (reviewSchema)
      .json (path)
      .rdd
      .zipWithUniqueId
      .map[(Int,String,Double)] { case (row,id) => (id.toInt, s"${row getString 2} ${row getString 0}", row getDouble 1) }
      .toDS
      .withColumnRenamed ("_1", "id" )
      .withColumnRenamed ("_2", "text")
      .withColumnRenamed ("_3", "overall")
      .as[ParsedReview]

  // Load the GLoVe embeddings file
  def loadGlove (path: String): Dataset[Embedding] =
    spark
      .read
      .text (path)
      .map  { _ getString 0 split " " }
      .map  (r => (r.head, r.tail.toList.map (_.toDouble))) // yuck!
      .withColumnRenamed ("_1", "word" )
      .withColumnRenamed ("_2", "vec")
      .as[Embedding]

// ----------------------- Refactor to functions begin --------------------

  type ReviewVector = (Int, List[Double])
  // [Id, Text, Overall] + [Word, Vec] => [Id, Vec]
  def mapToWordVecs (reviews: Dataset[ParsedReview]) (glove: Dataset[Embedding]): Dataset[ReviewVector] = {
    val tokenized = new Tokenizer()
        .setInputCol("text").setOutputCol("words")
        .transform(reviews)
      
    // Flatten
    val flattened = tokenized.select("id", "words")
      .as[(Int, List[String])]
      .flatMap { t => t._2.map { w => (t._1, w) }}
		  .withColumnRenamed ("_1", "id" )
		  .withColumnRenamed ("_2", "w")

    // Join with corresponding vector
	  flattened.join(glove, $"w" === $"word", "inner")
      .select("id", "vec")
      .as[ReviewVector]
  }

  def calcAvgVectors (vectors: Dataset[ReviewVector]): Dataset[ReviewVector] = {
    vectors.groupBy("id")
      .agg(collect_list("vec"))
      .as[(Int, List[List[Double]])]
		  .map { row => (row._1, row._2.transpose.map(x => x.sum/x.size.toDouble)) }
      .withColumnRenamed ("_1", "vId" )
  		.withColumnRenamed ("_2", "avgVec")
      .as[ReviewVector]
  }

  type ConvertedRating = (Int, Int)
  def mapRatings (review: Dataset[ParsedReview]): Dataset[ConvertedRating] = {
    review.select("id","overall").as[(Int, Double)]
		  .map { t => t._2 match {
  			case x if x <= 2.0  => (t._1, 0) // Negative
			  case x if x >= 4.0  => (t._1, 2) // Positive
			  case _ 				=> (t._1, 1) // Neutral
		  }
    } .withColumnRenamed ("_1", "rId" )
		  .withColumnRenamed ("_2", "rating")
      .as[ConvertedRating]
  }

  type FeatureLabel = (Vector, Int)
  def extractFeatures (review: Dataset[ParsedReview]) (glove: Dataset[Embedding]): Dataset[FeatureLabel] = {
    val labels = mapRatings(review)
    val feats = calcAvgVectors(mapToWordVecs(review)(glove))
      .map {row => (row._1, Vectors.dense(row._2.toArray))}
      .withColumnRenamed ("_1", "vId" )
  		.withColumnRenamed ("_2", "avgVec")
    
    feats.join(labels, $"vId" === $"rId", "inner")
		  .select("avgVec","rating")
		  .withColumnRenamed ("avgVec", "features" )
		  .withColumnRenamed ("rating", "label") 
      .as[FeatureLabel]
  }
// ----------------------- Refactor to functions end ----------------------

  def main(args: Array[String]) = {
    val d = 200	//This specifies the dimension of the input features - must match filename
    val glove  = loadGlove ("./gloves/glove_"+d+"d.txt")
    val reviews = loadReviews ("./data/reviews_Office_Products.json")

    println("Dimensions: "+d)

    val preparedData = extractFeatures(reviews)(glove)

    //TRANING MLP
    // Step 1. - Setup MLP
    val layers = Array[Int](d, 300, 200, 3)
	  val classifier = new MultilayerPerceptronClassifier()
  		.setLayers(layers)
  		.setBlockSize(512)
  		.setSeed(1234L)
  		.setMaxIter(50)

    // Step 2. - Split data into training and test sets
	  val splits = preparedData.randomSplit(Array(0.8, 0.2), seed = 1234L)
	  val train = splits(0)
	  val test = splits(1)

	  // Step 3. - k-fold cross-validation
	  val paramGrid = new ParamGridBuilder().build()
	  val crossval = new CrossValidator()
    crossval.setEvaluator(new MulticlassClassificationEvaluator()) //Default evaluator
	  crossval.setEstimator(classifier)
	  crossval.setEstimatorParamMaps(paramGrid)
	  crossval.setNumFolds(10) // k = 10

    // Step 4. - Train MLP
	  val model = crossval.fit(train)

    // Step 5. - Use trained MLP to review accuracy using the validation dataset
	  val result = model.transform(test)
	  val predictionAndLabels = result.select("prediction", "label")

    // Step 6. - Setup evaluators
    val f1Eval = new MulticlassClassificationEvaluator().setMetricName("f1")
	  f1Eval.setLabelCol("label")
	  f1Eval.setPredictionCol("prediction")

    val accEval = new MulticlassClassificationEvaluator().setMetricName("accuracy")
	  accEval.setLabelCol("label")
	  accEval.setPredictionCol("prediction")

    val wrEval = new MulticlassClassificationEvaluator().setMetricName("weightedRecall")
	  wrEval.setLabelCol("label")
	  wrEval.setPredictionCol("prediction")

    val wpEval = new MulticlassClassificationEvaluator().setMetricName("weightedPrecision")
	  wpEval.setLabelCol("label")
	  wpEval.setPredictionCol("prediction")

	  // Step 7. - Profit!
    val f1Res = f1Eval.evaluate(predictionAndLabels)
    println("F1: "+f1Res)

    val accRes = accEval.evaluate(predictionAndLabels)
    println("Accuracy: "+accRes)

    val wrRes = wrEval.evaluate(predictionAndLabels)
    println("Weighted Recall: "+wrRes)

    val wpRes = wpEval.evaluate(predictionAndLabels)
    println("Weighted Precision: "+wpRes)

    //REMAINING TASKS
    //Task 1. Write 5 unit tests
    //Task 2. Ensure that our functions are referentially transparent
    //Task 3. Do NOT use the Spark SQL interface

    spark.stop
  }
}