// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen
// 
// Group number: 21
//
// AUTHOR1: LABA
// TIME1: 3 hours 
// 
// AUTHOR2: AVAL
// TIME2: 6 hours 

package adpro

sealed trait Stream[+A] {
  import Stream._

  def headOption () :Option[A] =
    this match {
      case Empty => None
      case Cons(h,t) => Some(h())
    }

  def tail :Stream[A] = this match {
      case Empty => Empty
      case Cons(h,t) => t()
  }

  def foldRight[B] (z : => B) (f :(A, => B) => B) :B = this match {
      case Empty => z
      case Cons (h,t) => f (h(), t().foldRight (z) (f))
      // Note 1. f can return without forcing the tail
      // Note 2. this is not tail recursive (stack-safe) It uses a lot of stack
      // if f requires to go deeply into the stream. So folds sometimes may be
      // less useful than in the strict case
    }

  // Note 1. eager; cannot be used to work with infinite streams. So foldRight
  // is more useful with streams (somewhat opposite to strict lists)
  def foldLeft[B] (z : =>B) (f :(A, =>B) =>B) :B = this match {
      case Empty => z
      case Cons (h,t) => t().foldLeft (f (h(),z)) (f)
      // Note 2. even if f does not force z, foldLeft will continue to recurse
    }

  def exists (p : A => Boolean) :Boolean = this match {
      case Empty => false
      case Cons (h,t) => p(h()) || t().exists (p)
      // Note 1. lazy; tail is never forced if satisfying element found this is
      // because || is non-strict
      // Note 2. this is also tail recursive (because of the special semantics
      // of ||)
    }

  //Exercise 1
  def from (n:Int): Stream[Int] = cons(n, from(n+1))
  def to (n:Int): Stream[Int] = cons(n, to(n-1))
  def naturals: Stream[Int] = from(0)

  //Exercise 2
  def toList: List[A] = foldRight (List[A]()) (_::_)

  //Exercise 3
  def take(n: Int): Stream[A] = {
      if(n <= 0) { Empty }
      else {
          this match {
            case Empty     => Empty
            case Cons(h,t) => Cons(h, () => t() take(n-1))
          }
      }
  }
  def drop(n: Int): Stream[A] = {
      if (n <= 0) { return this }    
      this match {
          case Empty     => Empty
          case Cons(h,t) => t().drop(n-1)
      }
  }
  /*  Regarding 'naturals.take(1000000000).drop(41).take(10).toList'
    It terminates without memory exceptions fast due to being lazily evaluated. 
    Thus, only the first 51 naturals are evaluated, and not the rest of the Stream.
  */
  
  //Exercise 4
  def takeWhile(p: A => Boolean): Stream[A] = this match {
      case Empty     => Empty
      case Cons(h,t) => this.headOption() match {
          case None    => Empty
          case Some(x) => if (p(x)) { Cons(h,() => t() takeWhile(p)) } else Empty
      }
  }
  /*  Regarding 'naturals.takeWhile.(_<1000000000).drop(100).take(50).toList'
    This works for the same reason as the question in exercise 3. 
    Not the entire stream is evaluated, only the relevant first 150 naturals.
  */

  //Exercise 5
  //A StackOverflow error is expected as an infinite stream will never terminate.
  def forAll(p: A => Boolean): Boolean = {
    def np: A => Boolean = (x: A) => !p(x) // Inverted predicate
    !exists(np)
  }

  //Exercise 6
  def takeWhile2(p: A => Boolean): Stream[A] = { 
    foldRight[Stream[A]] (empty) ((head,tail) => if(p(head)) cons(head,tail) else empty)
  }

  //Exercise 7
  def headOption2 (): Option[A] = foldRight (None: Option[A]) ((h, _) => Some(h))

  //Exercise 8 The types of these functions are omitted as they are a part of the exercises
  def map[B] (f: A => B): Stream[B] = headOption() match {
    case None    => Empty
    case Some(h) => Cons(() => f(h), () => tail.map(f))
  }

  def filter(p: A => Boolean): Stream[A] = headOption match {
    case None    => Empty
    case Some(h) => if(p(h)) {Cons(() => h, () => tail.filter(p))} else Empty
  }

  def append[B >: A] (that: => Stream[B]): Stream[B] = 
    foldRight (that) {(h,t) => Cons(() => h,() => t)}

  def flatMap[B >: A] (f: A => Stream[B]): Stream[B] = 
    foldRight (Stream.empty[B]) { (h,t) => f(h).append(t) }

  //Exercise 09
  def find (p :A => Boolean) :Option[A]= this.filter(p).headOption
  /*
    For streams it makes sense as filter is lazy and will only evaluate the stream
    until finding an element satisfying the predicate (if it exists), were headOption
    defaults to None if a suitable element is not found. 
    Thus it's only a linear scan up until the requested element.

    For a list it does not make sense to interleave the filtering and head-checking. 
    Early termination is lost. The implementation describes a "find-all" behaviour for lists,
    but we might only be interested in the first element satisfying the predicate.
  */

  // Exercise 10
  def fibs: Stream[Int] = {
    def inner(prev: => Int, cur: => Int): Stream[Int] = 
      Cons(() => prev, () => inner(cur, prev + cur))
    inner(0,1)
  }

  //Exercise 11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case None         => empty
    case Some((a,s))  => cons(a, unfold(s)(f))
  }
    
  //Exercise 12
  def fib2: Stream[Int] = unfold (0, 1) { case (p, c) => Some(p, (c, p + c)) }
  def from2(n: Int): Stream[Int] = unfold (n) { x => Some((x+1,x+1)) }

  //Exercise 13
  def map2[B] (f: A => B): Stream[B] = unfold (this) {
      case Empty     => None
      case Cons(h,t) => Some(f(h()), t())
  }
  def take2(n: Int): Stream[A] = unfold ((this, n)) { case (s, i) => 
    if(i < 0) None else s match {
        case Empty     => None
        case Cons(h,t) => Some((h(), (t(), i-1)))
    }
  }
  def takeWhile3(p: A => Boolean): Stream[A] = unfold(this) { 
    case Cons(h,t) => if(p(h())) Some((h(),t())) else None
    case _         => None
  }
  def zipWith[B, C](s2: Stream[B])(f: (A, B) => C): Stream[C] = {
     unfold((this, s2)) {
       case(Cons(h1,t1), Cons(h2,t2)) => Some(f(h1(),h2()), (t1(),t2()))
       case _                         => None
     }
   }
}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: ()=>A, t: ()=>Stream[A]) extends Stream[A]

object Stream {

  def empty[A]: Stream[A] = Empty

  def cons[A] (hd: => A, tl: => Stream[A]) :Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def apply[A] (as: A*) :Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
    // Note 1: ":_*" tells Scala to treat a list as multiple params
    // Note 2: pattern matching with :: does not seem to work with Seq, so we
    //         use a generic function API of Seq

  //Exercise 1
  def from(n:Int): Stream[Int] = cons(n,from(n+1))

  def to(n:Int): Stream[Int] = cons(n,from(n-1))

  val naturals: Stream[Int] = from(0)  
}