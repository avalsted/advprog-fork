/* ADVANCED PROGRAMMING. Monadic Evaluators. Andrzej Wąsowski */
// Hand-in by Anders (aval@itu.dk) & Laurtiz (laba@itu.dk)

package adpro.monads
import scala.language.higherKinds

// Section 2.1 [Wadler]

// Wadler uses a langauge similar to Haskell to implement his evaluator. We will
// use scala.  This is Wadler's Term language implemented in Scala:

trait Term
case class Cons (value :Int) extends Term
case class Div (left :Term, right :Term) extends Term

// From now on we create one module (object) per section to avoid name clashes
// between different variants.

// This is the basic evaluator 
// (compare to the paper, to see whether you understand)
object BasicEvaluator {
  def eval (term :Term) :Int = term match {
    case Cons(a)  => a
    case Div(t,u) => eval (t) / eval (u)
  }
}

// Section 2.2 [Wadler] Variation one: Exceptions
 object ExceptionEvaluator {

   // an implementation of Wadler's types in Scala
   type Exception = String
   trait M[+A]
   case class Raise (e: String) extends M[Nothing]
   case class Return[A] (a: A) extends M[A]

  // An implementation of direct exception evaluator in Scala:
  def eval (term :Term) :M[Int] = term match {
    case Cons(a) => Return (a)
    case Div(t,u) => (eval (t)) match {
      case Raise(e)  => Raise (e)
      case Return(a) => (eval (u)) match {
        case Raise(e)  => Raise (e)
        case Return(b) => {
          if(b == 0) Raise ("divide by zero")
          else Return (a/b)
        }
      }
    }
  }
 }

// Section 2.3 [Wadler] Variation two: State
object StateEvaluator {
  type State = Int
  case class M[+A] (step: State => (A,State))

  def eval (term :Term) :M[Int] = term match {
    case Cons (a)  => M[Int] (x => (a,x))
    case Div (t,u) => M[Int] (x => {
      val (a,y) = eval(t).step(x)
      val (b,z) = eval(u).step(y)
      (a/b, z+1)
    })
  }
}

// Section 2.4 [Wadler] Variation three: Output
object OutputEvaluator {
  type Output = String
  case class M[+A] (o: Output, a: A)
  def line (a :Term) (v :Int) :Output = "eval(" + a.toString + ") <= " + v.toString + "\n"

  def eval (term :Term) :M[Int] = term match {
    case Cons (a)  => M[Int] (line(term)(a),a)
    case Div (t,u) => {
      val x = eval (t)
      val y = eval (u)
      val opt = x.a/y.a
      M[Int] (x.o + y.o + line(term)(opt), opt)
    }
  }
}

// Section 2.5 [Wadler] A monadic evaluator
// The following are two generic monadic interfaces (one for classes, one for
// meta-classes/objects) that we will use to type check our monadic solutions.
//
// We shall provide flatMap and map for our monads to be able to use for
// comprehensions in Scala.
//
// IMPORTANT: flatMap is called "(*)" in the paper.
trait Monad[+A,M[_]] {
  def flatMap[B] (k: A => M[B]) :M[B]
  def map[B] (k: A => B) :M[B]
}

// we will provide unit, as the paper does. This will be placed in a companion object.
trait MonadOps[M[_]] { def unit [A] (a :A) :M[A] }

// The above abstract traits will be used to constraint types of all our monadic
// implementations, just to ensure better type safety and uniform interfaces.
// Now we are starting to implement the monadic evaluator from the paper.
// Compare this implementation to the paper, and make sure that you understand
// the Scala rendering.

/// Section 2.6 [Wadler] Variation zero, revisited: The basic evaluator
object BasicEvaluatorWithMonads {
  // We enrich our M type with flatMap and map;
  // A flatMap is already in the paper (called *)
  // I add map, so that we can use for comprehensions with this type
  case class M[+A] (a: A) extends Monad[A,M] {
    def flatMap[B] (k: A => M[B]) :M[B] = k (this.a)
    def map[B] (k: A => B) :M[B] = M.unit (k (this.a))
  }

  // The paper also uses unit, so we put it in the companion object
  object M extends MonadOps[M] { def unit[A] (a : A) :M[A] = M[A] (a) }

  def eval (term: Term) :M[Int] = term match {
    case Cons (a)  => M.unit (a)
    case Div (t,u) => for {
      a <- eval (t)
      b <- eval (u)
      r <- M.unit (a/b)
    } yield r
  }
}

// Section 2.7 [Wadler] The monadic evaluator with exceptions
object ExceptionEvaluatorWithMonads {
  type Exception = String

  trait M[+A] extends Monad[A,M]{
    def flatMap[B] (k: A => M[B]) :M[B] = this match {
       case Raise (e) => Raise (e)
       case Return (a) => k(a)
    }

    def map[B] (k: A => B) :M[B] = this match {
      case Raise (e) => Raise (e)
      case Return (a) => Return (k(a))
    }
  }

  object M extends MonadOps[M] { def unit[A] (a : A) :M[A] = Return (a) }

  case class Raise (e: String) extends M[Nothing]
  case class Return[A] (a: A) extends M[A]

  def eval (term :Term) :M[Int] = term match {
    case Cons (a)  => M.unit (a)
    case Div (t,u) => (eval (u)).flatMap(b => 
      if(b == 0) Raise("divide by zero")
      else (eval (t).map(a => a/b))
    )
  }

  /** 
    The two monadic evaluators are very similar in the eval functions, and the cases for handling Cons and Div
    are almost identical. 
    The major difference between the two is that the monadic evaluator with exceptions have to handle the exception
    datatype and take invalid cases into consideration such that the right exception type can be returned.
  */
 }

 // Section 2.8 [Wadler] Variation two, revisited: State
 object StateEvaluatorWithMonads {
   type State = Int

   case class M[+A] (step: State => (A,State)) extends Monad[A,M] {

     // flatMap is bind or (*) in the paper
     def flatMap[B] (k :A => M[B]) = M[B] {
       x => { val (a,y) = this.step (x); k(a).step(y) } }

     def map[B] (k :A => B) :M[B] =
       M[B] { x => { val (a,y) = this.step(x); (k(a),y) } }
   }

   object M extends MonadOps[M] { def unit[A] (a : A) :M[A] = M[A] (x => (a, x)) }
   val tick: M[Unit] = M { x => ((), x + 1) } // Increment state as in the paper.

   def eval (term :Term) :M[State] = term match {
     case Cons (a)  => M.unit (a)
     case Div (t,u) => for {
       a <- eval(t)
       b <- eval(u)
       _ <- tick // increment number of divisions
       r <- M.unit(a/b)
     } yield r
   }

  /**
    In this case the two monadic evaluators are very similar, with the only difference being that
    we have to increment, using 'tick', the inner state of the monad.
  */
 }

 // Section 2.9 [Wadler] Output evaluator

 object OutputEvaluatorWithMonads {
   type Output = String

   case class M[+A] (o: Output, a: A) {

     // flatMap is (*) in [Wadler]
     def flatMap[B] (k :A => M[B]) = {
       val (x,a) = (this.o, this.a)
       val (y,b) = (k(this.a).o, k(this.a).a)
       M[B](x+y, b)
     }

     def map[B] (k :A => B) :M[B] = M[B] (this.o, k(this.a))
   }

   object M { def unit[A] (a : A) :M[A] = M[A] ("", a) }

   def line (a :Term)(v :Int): Output = "eval(" + a.toString + ") <= " + v.toString + "\n"
   def out(x: Output): M[Unit] = M(x, ())  // The "side-effect" as in the paper.

   def eval (term :Term) :M[Int] = term match {
     case Cons (a)  => M[Int] ((line (term)(a)),a)
     case Div (t,u) => for {
       a <- eval (t)
       b <- eval (u)
       r <- M.unit (a/b)
       m <- M[Int] ((line (term)(r)),r)
     } yield m
   }

  /**
    Very similar in the structure of the evaluator, however in this instance we use the additional
    methods 'line' and 'out' instead of the 'tick' from the other evaluator.
  */
 }